<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Lesson;
use App\Tag;
use App\Transformers\TagTransformer;
use Illuminate\Http\Request;

class TagsController extends ApiController
{
    protected $tagTransformer;

    public function __construct(TagTransformer $tagTransformer)
    {
        $this->tagTransformer = $tagTransformer;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($lessonId = null)
    {
        // Note, this did not go as planned
        // Due to error checking in video vs this style.
        // Ugly code ahead - non Jefferey Way!
        if ($lessonId) {
            if (!$lesson = Lesson::find($lessonId)) {
                return $this->respondNotFound('Resource not found');
            } else {
                $tags = $lesson->tags;
            }
        } else {
            $tags = Tag::all();
        }

        return $this->respond([
            'data' => $this->tagTransformer->transformCollection($tags->toarray())
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
