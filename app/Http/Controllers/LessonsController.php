<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Lesson;
use App\Transformers\LessonTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class LessonsController extends ApiController
{
    function __construct(LessonTransformer $lessonTransformer)
    {
        $this->middleware('auth.basic.once', [
            'except' => [
                'index'
            ]
        ]);
        $this->lessonTransformer = $lessonTransformer;
    }

    /**
     * @var LessonTransformer
     */
    protected $lessonTransformer;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$lessons = Lesson::all()->forPage(1,4);
        $limit = Input::get('limit') ?: 5;
        $lessons = Lesson::paginate($limit);

        return $this->respondWithPagination($lessons, [
            'data' => $this->lessonTransformer->transformCollection($lessons->all()),
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->input('title') or !$request->input('body')) {
            $message = 'Parameters failed validation for a lesson.';

            return $this->respondCreateError($message);
        }

        Lesson::create($request->all());

        $message = 'Lesson created!';

        return $this->respondCreated($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lesson = Lesson::find($id);
        if (!$lesson) {
            return $this->respondNotFound('Lesson does not exist');
        }

        return $this->respond([
            'data' => $this->lessonTransformer->transform($lesson)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
