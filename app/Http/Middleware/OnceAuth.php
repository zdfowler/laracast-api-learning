<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;


class OnceAuth
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Config::set('session.driver', null);
        \Config::set('cookie.driver', null);
        $fails = $this->auth->onceBasic();
        if ($fails) {
            return response()->json([
                'error' => [
                    'message' => 'You do not have access',
                    'status_code' => 401
                ]
            ]);
        }

        return $next($request);
    }
}
