<?php namespace App\Transformers;

/**
 * Created by PhpStorm.
 * User: Zac
 * Date: 12/11/2015
 * Time: 5:12 AM
 */
abstract class Transformer
{
    public function transformCollection($items){
        return array_map([$this, 'transform'], $items);
    }
    public abstract function transform($item);
}