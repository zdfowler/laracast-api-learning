<?php namespace App\Transformers;

/**
 * Created by PhpStorm.
 * User: Zac
 * Date: 12/11/2015
 * Time: 5:14 AM
 */

class TagTransformer extends Transformer
{
    /**
     * @param $tag
     * @return array
     */
    public function transform($tag)
    {
        return [
            'name' => $tag['name']
        ];
    }
}