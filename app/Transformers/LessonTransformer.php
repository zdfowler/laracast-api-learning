<?php namespace App\Transformers;

/**
 * Created by PhpStorm.
 * User: Zac
 * Date: 12/11/2015
 * Time: 5:14 AM
 */
class LessonTransformer extends Transformer
{
    public function transform($lesson){
        return [
            'title' => $lesson['title'],
            'body' => $lesson['body'],
            'active' => (boolean) 1

        ];
    }
}