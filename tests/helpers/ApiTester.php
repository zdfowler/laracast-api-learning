<?php

/**
 * Created by PhpStorm.
 * User: Zac
 * Date: 12/12/2015
 * Time: 1:13 PM
 */

use Faker\Factory as Faker;

abstract class ApiTester extends TestCase
{
    protected $fake;

    use FactoryZDF;

    public function createApplication()
    {
        putenv('DB_CONNECTION=sqlite_testing');

        return parent::createApplication();
    }

    public function __construct()
    {
        parent::__construct();
        $this->fake = Faker::create();
    }

    public function assertObjectHasAttributes($object, $fields)
    {
        foreach ($fields as $field) {
            $this->assertObjectHasAttribute($field, $object);
        }
    }

    public function getJson($uri, $method = 'GET', $parameters = [])
    {
        return json_decode($this->call($method, $uri, $parameters)->getContent());
    }


}


