<?php

/**
 * Created by PhpStorm.
 * User: Zac
 * Date: 12/12/2015
 * Time: 2:25 PM
 */
trait FactoryZDF
{
    protected $times = 1;

    protected function times($count)
    {
        $this->times = $count;

        return $this;
    }

    protected function make($type, array $fields = [])
    {
        while ($this->times--) {
            $stub = array_merge($this->getStub(), $fields);
            $type::create($stub);
        }
    }

    protected function getStub()
    {
        throw new BadMethodCallException('Create your own getStub method to declar your fields.');
    }
}