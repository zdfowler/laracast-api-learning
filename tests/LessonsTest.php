<?php

class LessonsTest extends ApiTester
{
    // Need to see how to do XCSRF in unit tests.
    use \Illuminate\Foundation\Testing\WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();
        \Illuminate\Support\Facades\Artisan::call('migrate');

    }

    public function testItFetchesLessons()
    {
        // arrange
        $this->make('App\Lesson');
        //factory(App\Lesson::class)->make();

        // act
        $this->getJson('api/v1/lessons');

//        $this->get('api/v1/lessons');
        // assert
        $this->assertResponseOk();
    }

    public function testItFetchesASingleLesson()
    {
        $this->make('App\Lesson');
        $lesson = $this->getJson('api/v1/lessons/1')->data;
        $this->assertResponseOk();
        $this->assertObjectHasAttributes($lesson, ['body', 'title']);
    }

    public function test404sIfALessonISNotFound()
    {
        $json = $this->getJson('api/v1/lessons/x');
        $this->assertResponseStatus(404);
        $this->assertObjectHasAttributes($json, ['error']);
    }

    // Note, the stub stuff will go away with generic factories
    public function testCreatesNewLessonGivenValidParameters()
    {
        $this->getJson('api/v1/lessons', 'POST', $this->getStub());
        $this->assertResponseStatus(201);
        // shoud it return the new ID ?? Test it here.
    }

    public function testThrows422IfNewLessonRequestValies()
    {
        $this->getJson('api/v1/lessons', 'POST');
        $this->assertResponseStatus(422);
    }

    public function getStub()
    {
        return [
            'title' => $this->fake->sentence,
            'body' => $this->fake->paragraph,
            'some_bool' => $this->fake->boolean
        ];
    }


}
