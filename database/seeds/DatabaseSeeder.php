<?php

use App\Lesson;
use App\Tag;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    private $tables = [
        'lessons',
        'tags',
        'lesson_tag'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->cleanDatabase();

        $this->call('LessonsTableSeeder');

        $this->call('TagsTableSeeder');
        $this->call('LessonTagTableSeeder');

        $this->call('UserTableSeeder');

        Model::reguard();
    }

    private function cleanDatabase()
    {
        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }
    }
}

class LessonsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 30) as $index) {
            Lesson::create([
                'title' => $faker->sentence(5),
                'body' => $faker->paragraph(4)
            ]);
        }
    }
}

class TagsTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 10) as $index) {
            Tag::create([
                'name' => $faker->word
            ]);
        }
    }
}


class LessonTagTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 30) as $index) {
            // Needs to be real lesson id and real tag id
            $lessonsIds = Lesson::lists('id')->toArray();
            $tagIds = Tag::lists('id')->toArray();
            DB::table('lesson_tag')->insert([
                'lesson_id' => $faker->randomElement($lessonsIds),
                'tag_id' => $faker->randomElement($tagIds),
            ]);
        }
    }
}


class UserTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        User::create([
            'name' => 'Zac',
            'email' => 'zdfowler@gmail.com',
            'password' => bcrypt('test1234')
        ]);
    }
}
